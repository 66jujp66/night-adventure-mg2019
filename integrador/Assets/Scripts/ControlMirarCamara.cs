﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlMirarCamara : MonoBehaviour
{
    Vector2 mouseMirar; //cuanto movimiento hizo la camara
    Vector2 suavidadV; //suaviza el movimiento
    public float sensibilidad = 5.0f; //cuanto tenes que mover el mouse para tener un movimiento del jugador
    public float suavizado = 2.0f;
    GameObject jugador;


    void Start()
    {
        jugador = this.transform.parent.gameObject;
    }

    void Update()
    {
        var md = new Vector2(Input.GetAxisRaw("Mouse X"), Input.GetAxisRaw("Mouse Y")); //cuanto cambia el movimiento 

        md = Vector2.Scale(md, new Vector2(sensibilidad * suavizado, sensibilidad * suavizado));

        suavidadV.x = Mathf.Lerp(suavidadV.x, md.x, 1f / suavizado); //interpolacion lineal
        suavidadV.y = Mathf.Lerp(suavidadV.y, md.y, 1f / suavizado);
        mouseMirar += suavidadV;
        mouseMirar.y = Mathf.Clamp(mouseMirar.y, -90f, 90f); //rota la camara hacia arriba y abajo
        transform.localRotation = Quaternion.AngleAxis(-mouseMirar.y, Vector3.right);
        jugador.transform.localRotation = Quaternion.AngleAxis(mouseMirar.x, jugador.transform.up); //rota al jugador y la camara de izq a der


    }
}
