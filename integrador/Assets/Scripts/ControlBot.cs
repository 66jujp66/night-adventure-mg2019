﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlBot : MonoBehaviour
{
    private int hp;

    // Use this for initialization
    void Start()
    {
        hp = 100;

    }

    public void RecibirDaño()
    {
        hp = hp - 25;
        this.Hablar();

        if (hp <= 0)
        {
            this.Desaparecer();
        }
    }

    public void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("proyectil"))
        {
            this.RecibirDaño();
        }
    }

    public void Desaparecer()
    {
        //GameObject explosion = GameObject.Find("explosion");
        //GameObject particulas = Instantiate(explosion, gameObject.transform.position, Quaternion.identity);

        //Destroy(particulas, 2);

        Destroy(gameObject);
    }

    public void Hablar()
    {
        Debug.Log("Hola...");
    }

}
