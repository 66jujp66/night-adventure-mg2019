﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlJugador : MonoBehaviour
{
    public float rapidez = 10.0f;
    public Camera camaraPrimeraPersona;

    public LayerMask capaPiso;
    public float fuerzaSalto;
    public CapsuleCollider col;
    private Rigidbody rb;


    // Start is called before the first frame update
    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked; //hace desaparecer el cursor y hace foco en la ventana de juego
        col = GetComponent<CapsuleCollider>();
        rb = GetComponent<Rigidbody>();

    }

    // Update is called once per frame
    void Update()
    {
        float movimientoAdelanteAtras = Input.GetAxis("Vertical");
        float movimientoCostados = Input.GetAxis("Horizontal");

        movimientoAdelanteAtras *= Time.deltaTime;
        movimientoCostados *= Time.deltaTime;

        transform.Translate(movimientoCostados, 0, movimientoAdelanteAtras);

        if (Input.GetKeyDown (KeyCode.Escape))
        {
            Cursor.lockState = CursorLockMode.None; //hace aparecer el cursor a su estado origial
        }

        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = camaraPrimeraPersona.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));

            //GameObject pro;
            //pro = Instantiate(proyectil, ray.origin, Quaternion.identity);
            //Rigidbody rb = pro.GetComponent<Rigidbody>();
            //rb.AddForce(camaraPrimeraPersona.transform.forward * 15, ForceMode.Impulse);
            //Destroy(pro, 5);
                                   RaycastHit hit;
            if (Physics.Raycast(ray, out hit) != false)
            {
                if (hit.distance < 5)
                {
                    if (hit.collider.name != "Piso")
                    {
                        //Debug.Log(hit.collider.name);
                        GameObject objetoTocado = GameObject.Find(hit.transform.name);
                        //GameObject objetoDisparado = GameObject.FindGameObjectWithTag("arma");
                        ControlBot scriptObjetoTocado = (ControlBot)objetoTocado.GetComponent(typeof(ControlBot));

                        if (scriptObjetoTocado != null)
                        {
                            scriptObjetoTocado.RecibirDaño();

                            //Destroy(objetoDisparado);

                        }
                    }
                }
            }
        }


    }

    private void FixedUpdate()
    {
        if (EstaEnPiso() && (Input.GetKeyDown(KeyCode.Space)))
        {
            rb.AddForce(Vector3.up * fuerzaSalto, ForceMode.Impulse);
        }
    }

    private bool EstaEnPiso()
    {
        Vector3 vector = new Vector3(col.bounds.center.x, col.bounds.min.y, col.bounds.center.z);
        return Physics.CheckCapsule(col.bounds.center, vector, col.radius * .9f, capaPiso);
    }

}
